The recommendations below are a general project directory structure for Data Science projects developed by IBH. It can serve as a template structure for various artifacts that are required as part of executing a data science project when following agile principles.

IBH Data Science Project Structure

Project Charter/ Exit Reports

The two documents under Docs/Project, namely the charter and exit reports are particularly important to consider. They help to define the project at the start of an engagement, and provide a final report to IBH Customers.

NOTE

In some projects, e.g. short term proof of principle (PoC) or proof of value (PoV) engagements, it can be relatively time consuming to create and all the recommended documents and artifacts. In that case, at least the Charter and Exit Report should be created and delivered to the customer. It is strongly recommended that the content of the documents be maintained, as they provide important information about the project and deliverables.

src

Stores the source code files for building the data pipeline (python, R etc). Unlike notebook files which are specific to data exploration and model training, the src directory will contain all code responsible for custom transformations and data movement activities. The sub-folders include

preparation: Contains code relevant to data ingestion tasks such as retrieving data from CSV, relational database, NoSQL, Hadoop etc. 

processing: Contains code relevant to data transformation of source data and to accomplish any data cleaning as required for the project. This is to satisfy one key requirement:  both off-line training and online prediction should use same data pipeline to reduce misalignment.

modeling: All code relevant to model building of ensemble models is stored here. Unlike the model sub-directory under notebooks, any data pipeline code to orchestrate an ensemble model is saved here.

test

Stores test cases (white and black box) relevant to model evaluation. The objective is to aid a seamless deployment to production as an API. 

Include test cases for asserting python source code, automated testing to support future changes and enhancements.

model

Folder for storing binary (json or other format) file for local use.

Use this folder for storing intermediate results only. For long term storage,  results should be stored in the model repository separately. Besides binary model files, this location also contains model metadata such as date, size of training data.

data

This Folder contains a subset of the data used for experiments. It includes both raw data and processed data for temporary use.

raw: Contains the raw results of the ingestion process,  generated from code in the “preparation” folder. 

Best Practice

It is recommended to to keep a local copy (data subset) rather than retrieving data from the remote data store each time. This guarantees platform isolation and guards against latency issues at source.

processed: Contains the post-processed results from the code executed in the “processing” folder. 

Best Pactice Tip

It is recommended to keep a permanent copy of processed results for verification and audit logging purposes.

notebook

This location contains all python notebooks including those developed during the exploratory data analysis (EDA) and modeling stages.

eda: Exploratory Data Analysis (aka Data Exploration) is a step for analyzing and recording observations for later steps. For short-term use, it should contain the results of exploration such as data distributions; For long-term reuse it should managed in a centralized place.

poc: Used only for maintaining code and/or data specific to a proof-of-concept. 

WARNING!!

Do NOT use the poc folder for maintaining code/data for long-term use as it will cleaned periodically.

modeling: Contains all core notebook artifacts including model building and training.

evaluation: Contains the results of model evaluation and performance for product owners. As a best practice, it is recommended to save the results of every model evaluation metric here.